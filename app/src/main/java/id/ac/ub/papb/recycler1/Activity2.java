package id.ac.ub.papb.recycler1;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class Activity2 extends AppCompatActivity implements View.OnClickListener {
    TextView tvNama2, tvNim2;
    Button bt2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        tvNama2 = findViewById(R.id.tvNama2);
        tvNim2 = findViewById(R.id.tvNim2);
        bt2 = findViewById(R.id.bt2);
        bt2.setOnClickListener(this);

        Intent intent = getIntent();
        String name = intent.getStringExtra("Nama");
        String nim = intent.getStringExtra("Nim");

        tvNama2.setText("Nama : " + name);
        tvNim2.setText("NIM : " + nim);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.bt2) {
            finish();
        }
    }
}